package com.example.androidtask.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.androidtask.api.remote.APIService
import com.example.androidtask.api.response.Item
import io.reactivex.disposables.CompositeDisposable

class TagDataSourceFactory(private val compositeDisposable: CompositeDisposable,
private val apiService: APIService): DataSource.Factory<Int, Item>() {

    val tagsDataSourceLiveData = MutableLiveData<TagDataSource>()
    private var queryString = ""

    override fun create(): DataSource<Int, Item> {
        val tagDataSource = TagDataSource(apiService, compositeDisposable,queryString)
        tagsDataSourceLiveData.postValue(tagDataSource)
        return tagDataSource
    }

    fun search(query: String){
        queryString = query
    }
}