package com.example.androidtask.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.androidtask.api.remote.APIService
import com.example.androidtask.api.response.Item
import com.example.androidtask.util.NetworkState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TagDataSource(private val apiService: APIService,
        private val compositeDisposable: CompositeDisposable, private val query: String) :
        PageKeyedDataSource<Int, Item>() {

    companion object {

    }
    val networkState = MutableLiveData<NetworkState>()
    override fun loadInitial(params: LoadInitialParams<Int>,
            callback: LoadInitialCallback<Int, Item>) {
        networkState.postValue(NetworkState.LOADING)
        fetchData(1, params.requestedLoadSize) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        networkState.postValue(NetworkState.LOADING)
        val page = params.key
        fetchData(page, params.requestedLoadSize) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>,
            callback: LoadCallback<Int, Item>) {
        networkState.postValue(NetworkState.LOADING)
        val page = params.key
        fetchData(page, params.requestedLoadSize) {
            callback.onResult(it, page - 1)
        }
    }

    private fun fetchData(page: Int, pageSize: Int, callback: (List<Item>) -> Unit) {
        val queryMap = HashMap<String, Any>()
        queryMap["page"] = page
        queryMap["pagesize"] = pageSize
        queryMap["order"] = "desc"
        queryMap["sort"] = "activity"
        queryMap["intitle"] = "perl"
        queryMap["site"] = "stackoverflow"
        if (query.isNotEmpty()) {
            queryMap["q"] = query
        }

        compositeDisposable.add(apiService.getTags(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ tagItems ->
                    networkState.postValue(NetworkState.LOADED)
                    callback(tagItems.items)
                }, { throwable ->
                    val error = NetworkState.error(throwable.message)
                    networkState.postValue(error)
                }))
    }
}