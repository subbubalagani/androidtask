package com.example.androidtask.ui.home

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidtask.adapter.TagAdapter
import com.example.androidtask.api.response.TagResponse
import com.example.androidtask.databinding.HomeFragmentBinding
import com.example.androidtask.datasource.TagDataSource
import com.example.androidtask.util.Status
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment : Fragment() {

    private lateinit var adapter: TagAdapter
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: HomeFragmentBinding
    private lateinit var inputMethodManager : InputMethodManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        inputMethodManager =
                activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        initAdapter()
        initSearch()
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        viewModel.tagsList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
        viewModel.getNetworkState().observe(viewLifecycleOwner, Observer {
            loadingProgressBar.visibility =
                    if (it?.status == Status.RUNNING) View.VISIBLE else View.GONE
        })
    }

    private fun initAdapter() {
        adapter = TagAdapter()
        binding.recyclerView.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter
    }

    private fun initSearch() {
        binding.searchItem.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updateListFromInput()
                true
            } else {
                false
            }
        }

        binding.searchItem.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                updateListFromInput()
                true
            } else {
                false
            }
        }
    }

    private fun updateListFromInput() {
        binding.searchItem.text?.trim()?.let {
            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            adapter.submitList(null)
            viewModel.searchTags(it.toString())
        }
    }
}
