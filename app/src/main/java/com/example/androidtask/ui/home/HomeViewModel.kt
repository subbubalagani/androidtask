package com.example.androidtask.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.androidtask.api.remote.APIService
import com.example.androidtask.api.response.Item
import com.example.androidtask.datasource.TagDataSource
import com.example.androidtask.datasource.TagDataSourceFactory
import com.example.androidtask.ui.base.BaseViewModel
import com.example.androidtask.util.NetworkState

class HomeViewModel : BaseViewModel() {
    var tagsList: LiveData<PagedList<Item>>
    private val pageSize = 20
    private var sourceFactory: TagDataSourceFactory =
            TagDataSourceFactory(compositeDisposable, APIService.getService())

    init {
        val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)
                .build()
        tagsList = LivePagedListBuilder<Int, Item>(sourceFactory, config).build()
    }

    fun searchTags(query: String){
        sourceFactory.search(query)
        tagsList.value?.dataSource?.invalidate()
    }

    fun getNetworkState(): LiveData<NetworkState> = Transformations.switchMap<TagDataSource, NetworkState>(
            sourceFactory.tagsDataSourceLiveData) { it.networkState }

}
