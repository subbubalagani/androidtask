package com.example.androidtask.api.remote

import com.example.androidtask.BuildConfig
import com.example.androidtask.api.response.TagResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface APIService {

    @GET("search/advanced")
    fun getTags(@QueryMap input: Map<String, @JvmSuppressWildcards Any>): Observable<TagResponse>

    companion object {
        fun getService(): APIService {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(APIService::class.java)
        }
    }
}